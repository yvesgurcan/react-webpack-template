# React/Webpack boilerplate

## Dependencies
* React Router
* Styled Components
* Babel config
* Prettier config

## Setup

`npm i`

## Development

`npm start`

Outputs a build file when running dev server--for faster deployment on GitHub Pages.